FROM node:10
COPY . /app
WORKDIR /app
RUN npm install && npm run build

FROM node:10-alpine
COPY --from=0 /app/dist /app/dist
COPY --from=0 /app/package.json /app
COPY --from=0 /app/package-lock.json /app
WORKDIR /app
RUN npm install --only=prod
EXPOSE 3500
CMD [ "npm", "start" ]
