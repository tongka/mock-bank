CREATE TABLE IF NOT EXISTS `account` (
    `account_no` VARCHAR(10) NOT NULL PRIMARY KEY,
    `name` VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS `card` (
    `card_number` VARCHAR(16) NOT NULL PRIMARY KEY,
    `owner_name` VARCHAR(100) NOT NULL,
    `cvv` VARCHAR(3) NOT NULL,
    `expiry_month` SMALLINT(6) NOT NULL,
    `expiry_year` SMALLINT(6) NOT NULL
);

CREATE TABLE IF NOT EXISTS `payment` (
    `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `card_number` VARCHAR(16) NOT NULL,
    `account_no` VARCHAR(16) NOT NULL,
    `amount` DOUBLE NOT NULL CHECK (`amount` >= 0),
    `timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (`card_number`) REFERENCES `card`(`card_number`),
    FOREIGN KEY (`account_no`) REFERENCES `account`(`account_no`)
);
