import dotenv from "dotenv";

dotenv.config();
const config = {
    port: parseInt(process.env.PORT || "3500", 10),
    database: {
        host: process.env.MYSQL_HOST || "localhost",
        user: process.env.MYSQL_USER || "root",
        password: process.env.MYSQL_PASSWORD || "",
        database: process.env.MYSQL_DATABASE || "bank"
    }
};

export default config;
