import Router from "koa-router";
import { validateCardController } from "../controllers/card.controller";
import { payController } from "../controllers/pay.controller";
import { getPaymentController } from "../controllers/payment.controller";

const router = new Router();

router.post("/card/validate", validateCardController);
router.get("/payment", getPaymentController);
router.post("/pay", payController);

export default router;
