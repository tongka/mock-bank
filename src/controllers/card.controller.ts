import { Length, validate } from "class-validator";
import { Context } from "koa";
import { cardModel } from "../models/card.model";
import { respondError, respondSuccess } from "./response";

class ValidateCardForm {
    @Length(16, 16)
    public cardNumber: string;

    @Length(3, 3)
    public cvv: string;

    constructor(body: any) {
        this.cardNumber = body.cardNumber;
        this.cvv = body.cvv;
    }
}

export const validateCardController = async (ctx: Context) => {
    const form = new ValidateCardForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const cardValidated = await cardModel.validateCard(form.cardNumber, form.cvv);
    if (!cardValidated) {
        return respondSuccess(ctx, { valid: 0 });
    }
    return respondSuccess(ctx, { valid: 1 });
};
