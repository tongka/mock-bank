import { Context } from "koa";

export function respondSuccess(ctx: Context, data?: any) {
    ctx.status = 200;
    ctx.body = data || { status: 1 };
}

export function respondError(ctx: Context, errors: string[]) {
    ctx.status = 400;
    ctx.body = errors;
}
