import { IsNumber, Length, validate } from "class-validator";
import { Context } from "koa";
import { cardModel } from "../models/card.model";
import { paymentModel } from "../models/payment.model";
import { respondError, respondSuccess } from "./response";

class PayForm {
    @Length(16, 16)
    public cardNumber: string;

    @Length(3, 3)
    public cvv: string;

    @Length(10, 10)
    public accountNo: string;

    @IsNumber()
    public amount: number;

    constructor(body: any) {
        this.cardNumber = body.cardNumber;
        this.cvv = body.cvv;
        this.accountNo = body.accountNo;
        this.amount = body.amount;
    }
}

export const payController = async (ctx: Context) => {
    const form = new PayForm(ctx.request.body);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const cardValidated = await cardModel.validateCard(form.cardNumber, form.cvv);
    if (!cardValidated) {
        return respondError(ctx, ["Invalid card information"]);
    }
    try {
        const paymentId = await paymentModel.createPayment(form.cardNumber, form.accountNo, form.amount);
        const payment = await paymentModel.getPayment(paymentId);
        respondSuccess(ctx, payment);
    } catch (e) {
        respondError(ctx, [e.message]);
    }
};
