import { IsNumberString, validate } from "class-validator";
import { Context } from "koa";
import { paymentModel } from "../models/payment.model";
import { respondError, respondSuccess } from "./response";

class GetPaymentForm {
    @IsNumberString()
    public id: string;

    constructor(body: any) {
        this.id = body.id;
    }
}

export const getPaymentController = async (ctx: Context) => {
    const form = new GetPaymentForm(ctx.request.query);
    const validationError = await validate(form);
    if (validationError.length > 0) {
        const responseError = validationError.map(error => Object.values(error.constraints))
            .reduce((prev, curr) => [...prev, ...curr]);
        return respondError(ctx, responseError);
    }
    const payment = await paymentModel.getPayment(parseInt(form.id, 10));
    if (payment) {
        respondSuccess(ctx, payment);
    } else {
        respondError(ctx, ["Payment not found"]);
    }
};
