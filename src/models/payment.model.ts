import { accountModel } from "./account.model";
import { cardModel } from "./card.model";
import { DatabasePool, databasePool } from "./database";
import { Payment } from "./entities/payment";

class PaymentModel {
    constructor(private pool: DatabasePool) { }

    public async getPayment(paymentId: number): Promise<Payment | null> {
        const conn = await this.pool.getConnection();
        return new Promise((resolve, reject) => {
            conn.query("SELECT * FROM `payment` WHERE `id` = ?", [paymentId], (err, rows) => {
                if (err) { return reject(err); }
                if (rows.length > 0) {
                    resolve(new Payment(rows[0]));
                } else {
                    resolve(null);
                }
                conn.release();
            });
        });
    }

    public async createPayment(cardNumber: string, accountNo: string, amount: number): Promise<number> {
        const card = await cardModel.getCard(cardNumber);
        if (!card) {
            throw new Error("Card not found");
        }
        const account = await accountModel.getAccount(accountNo);
        if (!account) {
            throw new Error("Receiver account not found");
        }
        if (amount <= 0) {
            throw new Error("Amount must be greater than 0");
        }
        const conn = await this.pool.getConnection();
        return new Promise((resolve, reject) => {
            conn.query("INSERT INTO `payment` (`card_number`, `account_no`, `amount`) VALUES (?, ?, ?)",
                [cardNumber, accountNo, amount], (err, res) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(res.insertId);
                    }
                    conn.release();
                });
        });
    }
}

export const paymentModel = new PaymentModel(databasePool);
