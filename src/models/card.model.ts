import { databasePool, DatabasePool } from "./database";
import { Card } from "./entities/card";

class CardModel {
    constructor(private pool: DatabasePool) { }

    public async getCard(cardNumber: string): Promise<Card | null> {
        const conn = await this.pool.getConnection();
        return new Promise((resolve, reject) => {
            conn.query("SELECT * FROM `card` WHERE `card_number` = ?", [cardNumber], (err, rows) => {
                if (err) { return reject(err); }
                if (rows.length > 0) {
                    resolve(new Card(rows[0]));
                } else {
                    resolve(null);
                }
                conn.release();
            });
        });
    }

    public async validateCard(cardNumber: string, cvv: string): Promise<boolean> {
        const card = await this.getCard(cardNumber);
        if (!card || card.cvv !== cvv) {
            return false;
        }
        return true;
    }
}

export const cardModel = new CardModel(databasePool);
