import { DatabasePool, databasePool } from "./database";
import { Account } from "./entities/account";

class AccountModel {
    constructor(private pool: DatabasePool) { }

    public async getAccount(accountNo: string): Promise<Account | null> {
        const conn = await this.pool.getConnection();
        return new Promise((resolve, reject) => {
            conn.query("SELECT * FROM `account` WHERE `account_no` = ?", [accountNo], (err, rows) => {
                if (err) { return reject(err); }
                if (rows.length > 0) {
                    resolve(new Account(rows[0]));
                } else {
                    resolve(null);
                }
                conn.release();
            });
        });
    }
}

export const accountModel = new AccountModel(databasePool);
