export class Payment {
    public id: number;
    public cardNumber: string;
    public accountNo: string;
    public amount: number;
    public timestamp: Date;

    constructor(row: any) {
        this.id = row.id;
        this.cardNumber = row.card_number;
        this.accountNo = row.account_no;
        this.amount = row.amount;
        this.timestamp = new Date(row.timestamp);
    }
}
