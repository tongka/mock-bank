export class Card {
    public cardNumber: string;
    public ownerName: string;
    public cvv: string;
    public expiryMonth: number;
    public expiryYear: number;

    constructor(row: any) {
        this.ownerName = row.owner_name;
        this.cardNumber = row.card_number;
        this.cvv = row.cvv;
        this.expiryMonth = row.expiry_month;
        this.expiryYear = row.expiry_year;
    }
}
