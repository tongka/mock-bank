export class Account {
    public id: string;
    public name: string;
    public accountNo: string;

    constructor(row: any) {
        this.id = row.id;
        this.name = row.name;
        this.accountNo = row.accountNo;
    }
}
