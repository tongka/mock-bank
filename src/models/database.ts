import mysql, { Pool, PoolConfig, PoolConnection } from "mysql";
import config from "../config";

export class DatabasePool {
    private pool: Pool;

    constructor(poolConfig: PoolConfig) {
        this.pool = mysql.createPool(poolConfig);
    }

    public getConnection(): Promise<PoolConnection> {
        return new Promise((resolve, reject) => {
            this.pool.getConnection((err, conn) => {
                if (err) { return reject(err); }
                resolve(conn);
            });
        });
    }
}

export const databasePool = new DatabasePool(config.database);
