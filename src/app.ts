import cors from "@koa/cors";
import Koa from "koa";
import bodyParser from "koa-body";
import logger from "koa-logger";
import config from "./config";
import router from "./routes/router";

(async () => {
    const app = new Koa();
    app.use(cors());
    app.use(logger());
    app.use(bodyParser());
    app.use(router.routes());
    app.listen(config.port, () => {
        console.log("Server started at port " + config.port);
    });
})();
